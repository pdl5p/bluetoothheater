#include "FontTypes.h"

// Font data for Tahoma 16pt
extern const uint8_t tahoma_16ptBitmaps[] PROGMEM;             // stored in program flash memory
extern const FONT_CHAR_INFO tahoma_16ptDescriptors[] PROGMEM;  // stored in program flash memory
extern const FONT_INFO tahoma_16ptFontInfo;

